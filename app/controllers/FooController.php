<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FooController
 *
 * @author Vitaly
 */
class FooController extends BaseController {
    //put your code here
    
    /**
     * 
     * @assert (1, 2) == 3
     * @assert (12, 23) == 35
     * @assert (11, 12) == 23
     * @assert (1, 29) == 30
     */
    public function summ($a, $b)
    {
        return $a + $b;
    }
    
}
